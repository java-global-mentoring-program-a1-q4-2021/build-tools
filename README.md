# Instructions to build project
Open terminal in the root of project.


## Via Gradle
- ```gradle clean``` cleans builds
- ```gradle compileTestJava``` compiles test code
- ```gradle test``` executes tests
- ```gradle compileJava``` compiles main code
- ```gradle build``` builds code'
- ```gradle run --args='Salam'``` runs code with arg
- ```gradle jar``` builds artifact admin-1.0.0.jar in /admin/build/libs


## Via Maven
- ```mvn clean``` cleans builds
- ```mvn test``` executes tests
- ```mvn install``` builds artifact admin-1.0.0.jar in /admin/target and put it in local repository
- ```mvn exec:java -pl admin -D"exec.args=Salam 312"``` runs code with arg
